from django.db import models
from datetime import datetime as dt

# Create your models here.

class ProjetoDjango(models.Model):
    #Aqui definimos as colunas que teremos em nosso banco de dados
    titulo = models.CharField(max_length=50)
    genero = models.CharField(max_length=50)
    ano = models.CharField(max_length=4)
    data = models.DateTimeField("Publicado em ", default=dt.now())

    def __str__(self):
        return self.titulo
